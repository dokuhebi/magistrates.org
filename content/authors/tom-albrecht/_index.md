---
title: Tom Albrecht III
role: Site Owner
avatar: images/tom-albrecht.jpg
bio: Former libertarian, devoted Christian, husband, and father
organization:
  name: Magistrates.org
  url: https://magistrates.org
social:
  - icon: envelope
    iconPack: fas
    url: mailto:dokuhebi@pm.me
  - icon: telegram
    iconPack: fab
    url: https://telegram.me/dokuhebi
  - icon: github
    iconPack: fab
    url: https://gitlab.com/dokuhebi/magistrates.org
---

## Biography

Tom Albrecht is a husband and father of 5 adults and teenagers. He grew up homeschooled in a Reformed Christian home and spent way too much time reading. A cybersecurity architect by vocation, he has spent a lifetime combining a scientific mindset while always focusing on the "why" in work life.
