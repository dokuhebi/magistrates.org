---
title: Tom Albrecht III
draft: false
role: Site Author
avatar: images/tom-albrecht.jpg
bio: Former libertarian, devoted Christian, husband, and father
organization:
  name:
  url:
social:
  - icon: envelope
    iconPack: fas
    url: mailto:dokuhebi@pm.me
  - icon: telegram
    iconPack: fab
    url: https://t.me/+AOY5pLf1-hU5ZGIx
  - icon: github
    iconPack: fab
    url: https://gitlab.com/dokuhebi/magistrates.org

weight: 1
widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:

  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment:
---

## About Magistrates.org

For decades, I've struggled with civil law, the Bible, and what establishes a legitimate civil authority. God clearly calls us to submit to rulers, but which ones, what establishes them, and what are the exceptions to submission?

Having been driven to the point of distraction, I decided to start writing out my thoughts on the topic. There is no real rhyme or reason to my plan, but I plan on working through various topics in scripture and throughout church history. Having lived most of my life in the United States, I understand the challenges and prejudices that I have, and while it is impossible to totally erase those prejudices, I am attempting to at least identify and address them.

## Contributing

One of my goals is for this to be a collaborate project along with readers who are working through the same ideas. If you would like to interact or contribute, join our [telegram group](https://t.me/+AOY5pLf1-hU5ZGIx). Since my vocation is in the computer world, the site is written in markdown, and the [source for the site is available](https://gitlab.com/dokuhebi/magistrates.org). You can submit an issue or merge request to offer fixes to any of the content.