---
title: The Doctrine of the Lesser Magistrates
description: A Review and Response
toc: true
authors: Tom Albrecht
tags: ["review"]
categories:
series:
date: '2023-07-14'
lastmod: '2023-07-17'
draft: false
---

## Introduction

This review of the book, "The Doctrine of the Lesser Magistrates: A Proper Response to Tyranny and a Repudiation of Unlimited Obedience to Civil Government", is written with great respect and gratitude to Matthew Trewhella. As I wrote in my [introduction to this website](../../../posts/intro-20230709), Mr. Trewhella addresses many of the questions I have had for years and writes in a context that I respect, that is, within a historical Protestant Christian context. The cover of the book has a representation of the King John signing the Magna Carta, an example of the interposition of lesser magistrates against the tyranny of the king. That event, the author argues, is an example of a Biblical response to tyranny versus lawless revolution and anarchy.

## The Interposition of Lesser Magistrates

Matthew Trewhella is the pastor of Mercy Seat Christian Church near Milwaukee, WI and frequently lectures of the topics of righteous resistance and the lesser magistrate doctrine. The book an easy read at about 100 pages, including appendices, and is not meant to be an exhaustive defense of Christian civil legal systems. Instead, the author seeks to introduce readers to an idea that even within Protestant Christianity has been largely forgotten, even though the idea has been articulated within the Protestant Church since the beginning of the Reformation.

The basic concept is defined in the first chapter:

> Historically, the practice of the church has been that when the state commands that which God forbids or forbids that which God commands, men have a duty to obey God rather then man. The Bible clearly teaches this principle. The lesser magistrate is to apply this principle to his office as magistrate. When an unjust degree is made by a higher authority, the lesser magistrate must choose to either join the higher magistrate in his rebellion against God, or stand with God in opposition to the unjust or immoral degree.

The term used for a lesser magistrate standing between the people over whom he rules and the higher ruler is "interposition" and the author gives Biblical examples of interposition in the Bible, including the Hebrew midwives standing between the mothers and Pharaoh's evil decree in Exodus 1, Saul's people standing between him and his son Jonathan in 1 Samuel 14. He also gives historical examples from both ancient and recent history. (Later in the book, the author has separate chapters on the magistrates of Magdeburg and on John Knox.)

To the author's credit, he gives examples of interposition and defiance towards a ruler as a serious act taken after negotiations have failed. He includes a quote from Patrick Henry, who,  frustrated with the acts of King George, stated:

> Sir, we have done everything that could be done to avert the storm which is now coming.

Interposition is no frivolous concept and while lesser magistrates should remember the tool in their toolbox, it should not be the first to reach for.

## The Objective Standard of the Law

After pointing out the historical and Biblical examples of the interposition of lesser magistrates, the author spends a few chapters focusing on the standards by which to determine when defiance of authority is valid. He writes, "When to disobey should not be left up to the whims of mere men." A doctrine of lesser magistrates necessitates an objective standard, as it does not simply a justification for disobedience over personal disagreements.

The author gives examples of why Western law has historically been based on Biblical law, e.g. laws against theft, murder, adultery, etc., then reminds the reader that the Biblical basis for our legal system has been all but lost in modern society and law.

## Honest Examples

Trewhella is not so naive to think that this doctrine is a panacea, as he does cite examples of bad interpositions. He mentions then San Francisco mayor Gavin Newsom performing homosexual marriages and being overruled eventually by the governor. He also describes the drama of Jeremiah's imprisonment and the weakness of King Zedekiah in the face of his lesser princes.

The author then correctly observes that the standard for Christians when taking sides is to compare the actions of the greater and lesser magistrates to the law of God. It is the standard by which we determine the rightness of actions.
## Personal Thoughts

### Cause and Effect

While the author does a good job of reminding his Western audience of the responsibilities they have to follow God's law despite what man's law is requiring, I think there's a missing element in the book of why we are where we are. Historically, the original government of the United States was founded on a representative form of government from the states to the federal government. This reflects the hierarchy of presbyterian government, with levels of representation from the local church session to the presbytery, synod, or General Assembly, with naming depending on denomination. It is not accident that these parallels exist.

With the growth of congregation polity in the United States, the systems of checks and balances reduced to various autonomous congregations, each governing themselves. The result of this failure to educate Christians on government, mutual submission, and superior and inferior courts has paralleled the failure of the populace to understand how those same concepts work in civil government. The result is individualism, which each church does what is right in its own eyes. That's not to say that the result is always chaos and immorality, but that the trend is to conclude that as long as my house is in order, it does not matter what is going on in the church across the street.

I believe that this book is a needed wake up call, but unless the reader understands the historical context for what Western society has forgotten the ultimate superiority of God's law and first brings it back into practice in the church, it is a hopeless exercise to try to implement those practices within civil government

### What Makes a Lesser Magistrate?

The title of the book refers to lesser magistrates, and the author does give a great number of examples of men and groups of men acting in the role of a lesser magistrate under a higher authority. However, I believe the biggest flaw of the book is the failure to define what a lesser magistrate *is* and how one identifies as being in that role. Two examples given of individuals as lesser magistrates are [Governor Publius Petronius](https://en.wikipedia.org/wiki/Publius_Petronius) under Emperor Caligula in Chapter 1 and police officers in Appendix C. In each of these examples, the lesser magistrate is a single individual appointed by a higher magistrate.

I think this question must be answered for the doctrine to be complete. On one hand, if a lesser magistrate is only in their position at the whim of the greater magistrate, then the legitimacy of their position is dependant on the approval of the that greater magistrate. On the other hand, if any one individual can declare themselves to be a lesser magistrate, then the doctrine is simply obfuscated anarchy.

The police officer example is very interesting, since the history of policing is such a complex subject and worth an article in its own right. The appendix provides simple examples of "good policing" and "bad policing", but does not delve into the complexities of the role of the police officer as a lesser magistrate. It simply accepts the role as given. It becomes even more complex in the chapter conclusion praising the Oathkeepers, which was made up of "military, police, and first responders, who pledge to fulfill the oath all military and police take to 'defend the Constitution against all enemies, foreign and domestic.'"

Does simply taking an oath making you a lesser magistrate in the author's eyes? These ambiguities in the book leave many questions.

## Conclusion

For a short treatise on the topic, the book is a good read, especially for those who still believe that God requires Christians to give full obedience to the evil dictates of our civil rulers. I believe the book has value, even if there are many questions that are left unanswered. Those questions would not be asked unless the author had started the conversation, and I am excited to continue to dig into the consequences of the ideas from Mr. Trewhella.