+++
author = "dokuhebi"
title = "Security in Orania"
date = "2023-08-05"
description = "Private Security in a Small Town"
tags = [
    "orania", "policing",
]
+++

It's no secret I'm fascinated with Orania in western South Africa. I have also done some deep dives in the history of policing, a concent that is entirely foreign to Biblical civil law, but so ubiquitous in Western society that conservatives cannot imagine a world without it.

This video popped up in my Youtube when I was doing my research on my "[Corporations as Lesser Magistrates" article](../../docs/articles/corporation-as-lesser-magistrate), and I thought they did great job of describing how they're tackling the needs for security in their town, employing a more community focused process.

(Even though the video is in Afrikaner, it is one of the few Orania videos that is actually subtitled in English.)

{{< youtube 3x4_dLVHC90 >}}