---
title: Civil Authorities in the Old Testament
description: A brief survey of various civil authorities from pre-flood to post-exile
toc: true
authors: Tom Albrecht
tags:
categories:
series:
date: '2023-08-05'
lastmod: '2023-08-05'
draft: false
---

## Pre Flood

Kings and other rulers are described throughout scripture, with the first arguably being Cain, who after murdering his brother left and "built a city, and called the name of the city after the name of his son — Enoch."[^1] While not given a title, it was clear his was the patriarch of his city and his descendants are listed in the next few verses.

As for his brother Seth, there is not much described by way of a civil government. In the generations that follow to Noah and the flood, mankind is described almost as a form af anarchy, and and evil one at that. When God establishes his covenant with Noah after the flood, there is the first glimpses of a criminal law:

> Whoever sheds man’s blood,\
> By man his blood shall be shed;\
> or in the image of God\
> He made man.[^2]

While the words are not necessarily proscriptive, there is a foreshadowing here of more detailed commands that are laid out in the Mosaic Law. Still, nothing specific about who is responsible for the resulting shedding of blood. (Also, we can see another foreshadowing of the legitimacy of the Avenger of Blood[^3]).

## Post Flood

While arguments can be made that Nimrod was the ruler over Babel, there is nothing explicit about that in Genesis 11. The decision to build a tower is recorded as a collective decision ("Come, let us build ourselves a city, and a tower whose top is in the heavens"[^4]). We then see more of Noah's descendants until we reach Abraham. At that point, we meet the Pharaoh of Egypt.

With the introduction of Pharaoh to the story, we meet a "lord of lords", since the princes of Egypt were the ones who brought Abram and Sarai to him, implying his position as superior. Their interaction is brief, albeit dramatic, but ultimately, Abram ends up back in Canaan. (Later, Pharaoh's power over Egypt grew much greater due to the shrewd actions of Joseph, forcing the Egyptians to sell their land to Pharaoh in exchange for food to keep them alive.)

Genesis 14 has the first references to kings, mentioning the rulers of various parts of Canaan and the wars that happened between them. There is also the mysterious King of Salem, Melchizedek, "Priest of the Most High God". Abram interacted with Melchizedek and the King of Sodom as a peer, establishing his legitimacy as a civil ruler, even though lacking the title of king.

The kings of the nations of Canaan and the Pharaoh of Egypt later become a sticking point for Israel hundreds of years later, when they ask the prophet and judge Samuel to "make us a king to judge us like all the nations."[^5]

Abram (and his patriarch descendants) acted as a civil authority over the people under him. He only had two direct descendants, so the 318 trained servants should be considered both his subjects and his employees. I'm not sure how to reconcile those numbers with the 70 descendants of Jacob that went to Egypt during the famine. I speculate that the number is not the full complement of people that migrated into Egypt.

Skipping forward to Moses, we see him acting as a civil ruler over Israel after their exodus from Egypt. Even before receiving the law, he ruled over the people. Jethro's advice to delegate the work of judgement to a hierarchy of men occurred before God gave his law on Mount Sinai. Joshua likewise, as Moses successor, ruled over Israel, receiving direct revelation from God.

However, no successor to Joshua was appointed by God, and Israel went through a series of unfaithfulness, subjugation, and repentance[^6]. At these times, God would raise up judges who would overthrow their oppressors, then judge Israel until their death. Redemption followed by rule would be the pattern until Saul was anointed as King. (This pattern makes the desire by Israel for a permanent king almost understandable.)

## Kings of Israel

The anointing of Saul as king of Israel introduced the kingdom to a long string of monarchs who ruled with very little oversight. Failure to rule by God's rules led to God's direct intervention[^7]. Saul's dynasty was short, but his successor David had a long reign, although during that time, two of his sons, Absalom and Adonijah made a play for the throne, both ending up being political gambits that didn't work out[^8].

The Old Testament kings seemed to have ultimate authority, but had to walk a fine line with upsetting their subjects. The attempts by David's two sons for the throne demonstrated that the people seemed to support whichever man had the wind in their sails. The people seemed to accept the new kings, yet also accepted David back when usurpers were defeated.

The ultimate power was oddly exercised. David slept with Uriah's wife, Bathsheeba. Ahab and Jezebel murdered Naboth for his vineyard.

## Exile

The actions of the captive Israelites during the exile deserves its own post. The relationship between Esther and Daniel and the authorities they served were interesting in their right, and it's clear it was without consent, as they were captives in a foreign land, but they still provided great value and blessing to the kings they served.

## Post Exile

After the captivity of Jerusalem, Cyrus, king of Persia, sent the tribes of Judah, Benjamin, and Levi to Jerusalem to rebuild the temple. The civil authorities represented were the "heads of the fathers’ houses of Judah and Benjamin".

> Then the heads of the fathers’ houses of Judah and Benjamin, and the priests and the Levites, with all whose spirits God had moved, arose to go up and build the house of the Lord which is in Jerusalem.[^10]

The people were led by Zerubbabel, grandson of the captured king of Judea, Jeconiah. It was Jeconiah whom Jeremiah cursed, saying that "none of his descendants shall prosper, sitting on the throne of David, and ruling anymore in Judah"[^9] Although Jeconiah did have a claim to the throne, scripture does not record that he ever claimed it, instead asserting leadership of Judea as a governor and lesser magistrate under Cyrus. The place of Zerubbabel in Old Testament history is fascinating, as it is clear that he was faithful to God, and that God had special blessings for him.

> ‘In that day,’ says the Lord of hosts, ‘I will take you, Zerubbabel My servant, the son of Shealtiel,’ says the Lord, ‘and will make you like a signet ring; for I have chosen you,’ says the Lord of hosts.”[^11]

The return involved the migration of over 42,000 people, and they immediately began to offer sacrifices in Jerusalem and began plans to rebuild the temple, "according to the permission which they had from Cyrus king of Persia."[^12]

The inhabitants of the land were upset with the return of their Jewish people and the rebuilding of the temple, and sent letters of accusation to Cyrus that their work was not merely religious, but would lead to rebellion against Cyrus. Cyrus expressed concern at their accusations and ordered the people to cease building the walls of Jerusalem (an assertion of civil sovereignty).

It was years later under the reign of Artaxerxes that Nehemiah was able to convince him to allow the walls of Jerusalem to be rebuilt. Apparently, Artaxerxes did not have the same concerns that Cyrus had regarding the rebuilding of Jerusalem and the risk of rebellion. So, Nehemiah went to Jerusalem and, as the governor, rebuilt the city of Jerusalem and the walls.

This ends the history of the people of Jerusalem recorded in scripture, but we know that any concerns the Persian empire may have had regarding Jerusalem were done away with when Alexander the Great concerned the Mediterranean world. No sovereign king sat on a throne in Jerusalem after the Babylonian captivity.


[^1]: [Genesis 4:16](https://www.biblegateway.com/passage/?search=Genesis+4%3A16&version=NKJV)
[^2]: [Genesis 9:6](https://www.biblegateway.com/passage/?search=Genesis+9%3A6&version=NKJV)
[^3]: [Numbers 35](https://www.biblegateway.com/passage/?search=Numbers+35&version=NKJV)
[^4]: [Genesis 11:4](https://www.biblegateway.com/passage/?search=Genesis+11%3A4&version=NKJV)
[^5]: [1 Samuel 8:5](https://www.biblegateway.com/passage/?search=1+Samuel+8%3A5&version=NKJV)
[^6]: [Judges 2](https://www.biblegateway.com/passage/?search=Judges%202&version=NKJV)
[^7]: [1 Samuel 13:14](https://www.biblegateway.com/passage/?search=1+Samuel+13%3A14&version=NKJV), [2 Samuel 12:14](https://www.biblegateway.com/passage/?search=2+Samuel+12%3A14&version=NKJV)
[^8]: [2 Samuel 15-18](https://www.biblegateway.com/passage/?search=2%20Samuel%2015&version=NKJV), [1 Kings 1](https://www.biblegateway.com/passage/?search=1+kings+1&version=NKJV)
[^9]: [Jeremiah 22:28-30](https://www.biblegateway.com/passage/?search=Jeremiah+22%3A28-30&version=NKJV)
[^10]: [Ezra 1:5](https://www.biblegateway.com/passage/?search=Ezra+1%3A5&version=NKJV)
[^11]: [Haggai 2:23](https://www.biblegateway.com/passage/?search=Haggai+2%3A23&version=NKJV)
[^12]: [Ezra 3:7](https://www.biblegateway.com/passage/?search=Ezra+3%3A7&version=NKJV)
