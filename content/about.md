+++
title = "About"
description = "What are magistrates?"
date = "2023-07-08"
aliases = ["about-us", "contact"]
author = "dokuhebi"
+++

For many years, I've struggled with civil law, the Bible, and the struggle I've been dealing with for decades on what makes a legitimate authority. God clearly calls us to submit to civil authorities, but which ones, what establishes them, and what are the exceptions to submission?

While this site is currently in blog format, I expect that posts will be edited over time as my thinking coalesces. I'll do my best to note where changes have occurred.
