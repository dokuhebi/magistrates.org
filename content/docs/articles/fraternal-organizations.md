---
title: The Fraternal Organization as Lesser Magistrate
description: Recoverting the Lost Tools of Brotherhood
toc: true
authors: Tom Albrecht
tags: ["fraternity", "magistrates"]
categories:
series:
date: '2023-10-19'
lastmod: '2023-10-19'
draft: false
---

## Introduction

If playing "word association", a person hearing the word "fraternity" would readily respond with "college", "beer", and "party", and it would be justified. While various fraternal organizations have histories that go back over a hundred years, their popularity has waned, with the exception of college "frats" and sororities.

However, fraternities were born out of the understanding that men working together could accomplish much more than those working separately. Within the philosophy of American Individualism (compounded by Christian Individualism), it is understandable why communities that have historically worked together to accomplish goals (blue collar laborers, Roman Catholics, Jewish, etc.) have been associated with the most [well-known fraternal organizations in the United States](https://en.wikipedia.org/wiki/List_of_North_American_ethnic_and_religious_fraternal_orders#Religious), while Protestants number very few. We just don't work well together.

## Focus: The Knights of Columbus

The Knights of Columbus, one of the largest Catholic fraternal organizations, was [founded out of adversity](https://www.ncronline.org/knights-columbus-financial-forms-show-wealth-influence).

> The Irish Catholics who poured into the United States by the hundreds of thousands in the mid-19th century, hoping to escape famine and professing a faith that was despised by many, strained to gain a toehold in a hostile culture.

At the time, the immigrants recognized that while normal life in a new culture was challenging, a family who had to deal with tragedy was often less destitute. Many fathers worked dangerous jobs, and a death or debilitating injury could result in his family ending up destitute. Rather than leave these families in the hands of the ecclesiastical church, a priest named Michael McGivney challenged the men in his parish in New Haven, Connecticut to come together to mutually support each other in life and sometimes in death.

From that humble challenge, the brotherhood has grown to millions of members around the world, organized into local councils, recognizing the balance between a global vision and local action.

## The Hostility of the World

In a very real sense, the challenges that the Irish felt in the late 19th century are being felt by all Christians today who declare Christ to be King. While there is much fault on behalf of generations of Christians who have lost their saltiness over the last two centuries in the United States, it is clear that our long-time opponents have coalesced around a hatred for God and are working strategically to wipe His people from the Earth. This is not simply a general attitude of antichrist, but a strategic vision to remove God from the throne of the universe and to replace Him with their king, Satan.

Many responses have been proposed by Christians to address this growing hostility. Some want to escape, working with others to form intentional communities as far from the hostility as possible. Others focus on politics, working to stem the tide by being a voice within a hostile system.

The fraternal organization offers another option for Christian men to relearn the power they have in cooperation. This is visually demonstrated by the Knights of Columbus in their [exemplification ceremony](https://www.kofc.org/apps/resources/10991-exemplification-of-charity-unity-and-fraternity.pdf) with single strings and a rope. Each initiate is given a piece of string and told to break the string, which they can easily do. They are then shown a cable made of strings that cannot be broken. This ritual demonstrates the truth from scripture that:

> Though one may be overpowered by another, two can withstand him. And a threefold cord is not quickly broken.[^1]

To be clear, not everyone is called to formal fraternity. While the principles and truth of the benefit of unity is clear, the bonds we choose to establish are unique to each person. Every marriage is a separate bond, every member subject to a particular church, and every child dependant on his parents. However, in these hostile times, the value and blessing of unity and fraternity will become more and more apparent.

## A Lesser Magistrate

As I wrote previously, I believe is it clear that at times in history, [God has called lesser magistrates to stand up to the higher earth powers](../doctrine-of-the-lesser-magistrates-review). Lesser magistrates have manifested themselves in many forms throughout history, and the [corporation is one of those forms](../corporation-as-lesser-magistrate). While not explicitly a civil organization, these organizations have a hierarchy, a locality, and a mission that meets the qualification. This is no small thing, as the vast majority of the men in the United States have lost the tools of civics, and these organizations allow men to strengthen those atrophied skills.

Over the centuries of Rome's decline, the power being exerted from the emperors began to wane. As a result, power vacuums formed across Europe. Those vacuums were filled by men united by leaders who had one claim to leadership... they led.

The [Merovingian Dynasty](https://en.wikipedia.org/wiki/Merovingian_dynasty) was one such fraternity in an area previously ruled by Rome, but once abandoned, they grew to rule all of France. They did not wait for Rome's permission to govern, but established a capacity to rule themselves when Rome disappeared from Western Europe.

Instead of waiting for permission from our absentee rulers, a fraternal organization can help men reclaim the skills to govern.

## Rejecting Isolation

Andrew Isker devotes a chapter in *The Boniface Option*[^2] to the tragedy he calls "The Atomized Man".

> Nothing characterizes the dystopia of our modern, liberal consumerist society more than the profound loneliness, isolation, and alienation experienced by young men and women. It is extremely common for young men (and even young women) to have no friends they regularly see in person.

COVID did not create our society's isolation, but simply manifested the isolation that already existed in our society. The Church was not immune to this, and when governments started encouraging churches to close their doors, they were more than happy to switch to virtual services. In reality, their services were already individualistic experiences. That they happened to be in the same building was coincidental and the migration to individual consumer worship was an easy transition to make.

This isolation is not happenstance. Our society encourages isolation, knowing that men working together with a common vision is a cord that cannot be broken. Since our enemies want us broken, they continue to devise new and attractive ways to keep us separated.

Isker continues:

> All of this goes much deeper than making life miserable for individuals. It is an intentional instrument of social and political control. This is because close relationships between men, where deep bonds of love and loyalty are forged are *essential* to life. (emphasis his)

and

> It is within the socially engineered world of atomized, isolated individualism you dwell. It is *designed* for you to feel alone and powerless. This is a feature not a bug. (emphasis his)

In the face of an enemy strategically working to keep you isolated, the only response is a deliberate strategy of fraternity.

## Not a Replacement for the Ecclesiastical Church

While Isker's book is light on formal solutions, the natural response is that the local church is the framework intended to provide those fraternal bonds. While this is partly true, fraternal bonds are both smaller and bigger than the local congregation. The local church is a necessary element of the Christian life[^3], but the local church is made up of men, women, and families, for the purpose of worshiping God and serving each other. The responsibilities of the church leadership is for elders to focus on "prayer and to the ministry of the word" and for deacons to serve those in need[^4]. Business, civil, and fraternal organizations are outside the responsibility of the local ecclesiastical church, although their missions are complementary and not in competition. Since not all in the church are called to brotherly fraternity, the responsibilities for Christian fraternity is outside that scope. The Knights of Columbus recognize this and in fact, the organization is a large contributor to Catholic missions like the churches, charitable missions, and education.

Likewise, local congregations and denominations have valid distinctions, not all of them theological. The mission of an inner city church will be different from a rural church. Baptists, Charismatics, and Lutherans have different histories and cultures and will not be unifying in the near future. Regardless of those differences, the Church is comprised of men from those congregations and a fraternal organization founded on the creed that Christ is King can cross those boundaries.

There is precedence for this cross-denominational Protestant cooperation during the American Revolution. While the Congregationalists and Presbyterians were separate denominations, they [united around their vision of religious separation from the King of England](https://www.google.com/books/edition/Historical_and_Political_Reflections_on/cHxbAAAAQAAJ?hl=en&gbpv=1&pg=PA54&printsec=frontcover). As a result, their cross-denominational cooperation united them in spite of the differences of their individual congregations.

## Conclusion

The isolation of Christian men is an anomaly in history. In the past, men have recognized the Kingship of Jesus Christ, been fed by the means of grace from the Church through preaching and the sacraments, and then worked together in various missions to forward the kingdom. Those acts of kingdom building included founding universities, starting businesses, colonizing the New World. These were major undertakings and required the brothers to work together.

Today, the skill of fraternity is needed simply to stand together against tyranny, but it is a skill that has atrophied. While it is much less of a challenge to organize one's self or one's family, buy a farm and raise a few chickens, the challenges of survival, let alone a counteroffensive, require brothers who will live and give their lives for their brothers.

I am not oblivious to the challenges it would take to take the idealism of fraternity and turn it into a concrete reality, but I believe that God has allowed the recent acceleration of tyranny and antichrist to show His people their need for trusted brothers to survive and thrive. Christians need to take back their birthright as adopted children that they have pawned off to governments and to local church leaders.

Any undertaking would need to start off humbly, to force men to learn to work together on projects bigger than they can accomplish individually, but faithful devotion to that work will result in blessings and greater responsibility[^5].

[^1]: Isker, A. (2023). The boniface option: A strategy for Christian counteroffensive in a post-Christian nation. Gab AI Inc.
[^2]: [Ecclesiastes 4:12](https://www.biblegateway.com/passage/?search=Ecclesiastes+4%3A12&version=NKJV)
[^3]: [Hebrews 10:24-25](https://www.biblegateway.com/passage/?search=hebrews+10%3A24-25&version=NKJV)
[^4]: [Acts 6:1-5](https://www.biblegateway.com/passage/?search=acts+6%3A1-5&version=NKJV)
[^5]: [Matthew 25:29](https://www.biblegateway.com/passage/?search=Matthew+25%3A29&version=NKJV)