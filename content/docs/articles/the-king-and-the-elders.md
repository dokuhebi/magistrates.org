---
title: The King and the Elders
description: The King and the Elders
toc: true
authors: Tom Albrecht
tags:
categories:
series:
date: '2023-07-14'
lastmod: '2023-07-14'
draft: false
---

In looking through the lives of the kings of Israel, I was struck by the relationship between the king and the "Elders of Israel". The title is first used in Exodus 3, and demonstrates that while God was clearly speaking through Moses as His prophet, there was an established hierarchy of leaders that existed at the time of Israel in Egypt prior to Moses. When Moses is first commissioned by God, the interaction between Israel and Pharaoh was initiated by Moses and the elders.

> Then they will heed your voice; and you shall come, you and the elders of Israel, to the king of Egypt; and you shall say to him, ‘The Lord God of the Hebrews has met with us; and now, please, let us go three days’ journey into the wilderness, that we may sacrifice to the Lord our God.’[^1]

God recognized that Moses was being sent to civil structure that already existed, even in slavery. The elders weren't simply leaders from the tribes of Israel, since by the end of the book of Exodus there were at least 70 numbered[^2] as commanded by God.

> So the Lord said to Moses: “Gather to Me seventy men of the elders of Israel, whom you know to be the elders of the people and officers over them; bring them to the tabernacle of meeting, that they may stand there with you.

> So Moses went out and told the people the words of the Lord, and he gathered the seventy men of the elders of the people and placed them around the tabernacle. Then the Lord came down in the cloud, and spoke to him, and took of the Spirit that was upon him, and placed the same upon the seventy elders; and it happened, when the Spirit rested upon them, that they prophesied, although they never did so again.[^3]

 70 men making up the elders of Israel stays consistent through the centuries, and later was referred to as the Sanhedrin, well-known for the trial of Jesus, Peter and John, and Paul.  Of note, the 70 were separate from the heads of the tribes , as they are identified separately from the elders of Israel in 1 Kings[^4].

We often hear the pejorative statement about the times of the judges, between the death of Joshua and the anointing of Saul as king that "In those days there was no king in Israel; everyone did what was right in his own eyes."[^5]. My assumption has always been that there was pure anarchy, but the elders of Israel existed before and during the reign of the judges and the kings. Even more striking is that the kings were installed with the consent of those elders.

## Kings Anointed by the Elders

The elders of Israel realized that Samuel, the last judge, had sons that were not fit to act as judge and pleaded with Samuel to find a king[^6]. Later, during the house wars between Saul and David, Abner, the commander of Saul's army, saw that God had appointed David to replace Saul as the king of Israel. When he saw the moment was right, he consulted with the elders of Israel:

> Now Abner had communicated with the elders of Israel, saying, “In time past you were seeking for David to be king over you. Now then, do it! For the Lord has spoken of David, saying, ‘By the hand of My servant David, I will save My people Israel from the hand of the Philistines and the hand of all their enemies.’ ”[^7]

Abner clearly saw that the royal title was granted by the elders. When David was anointed, he first makes a covenant with the elders and is the elders of Israel who anoint David, demonstrating publicly their rejection of Saul as king. Scripture records that action marking the beginning of David's reign as king.

Later, when Absalom took over David's throne, the elders of Israel supported him. In order to avoid war and bloodshed in Jerusalem, David left the city and Absalom took over the throne with the support of the elders of Israel. Of note, when David's friend Hushai advised David to flee further, that he referred to Absalom as "the king"[^8]. It was, therefore, the recognition of the elders that validated the claim of the king.

## Elders of Israel in the New Testament

The Elders of Israel are mentioned three times in the New Testament, in the gospels and in the book of Acts. The first is, of course, Jesus trial before the Sanhedrin.

The second is when Peter and John were arrested in Acts 4. After the arrest, Peter addressed the elders of Israel in Jerusalem and was told by them to keep quiet about Jesus. Peter and John promptly ignored the request and were arrested again. Again, they were brought to the council (or the Sanhedrin) "with all the elders of the children of Israel" [^9].

The third is Paul's examination after being arrested in the temple[^10]. Realizing that he was not going to get fair treatment, he lobbed a rhetorical grenade in their midst, dividing the group over their different beliefs on the resurrection.

As a conquered nation, the elders of Israel had a dysfunctional relationship with their people. The Romans were in charge, and they governed via local governors and puppet kings. They did maintain a level of power, most likely since their position was ordained in the Mosaic law, and even though conquered, the Romans allowed a level of autonomy for subject nations as long as that autonomy did not include insurrection against Rome.

## Random Thoughts

Wikipedia has an [in-depth article on the Sanhedrin](https://en.wikipedia.org/wiki/Sanhedrin), which offers the literal translation of the word as "sitting together", and seems to be a later continuation of the concept, but the word wasn't used until after the Babylonian exile. Interestingly, the word συνέδριον ("Sanhedrin") is used in the NKJV headers (i.e. "Jesus Faces the Sanhedrin" in Matthew 26), but the Greek is translated in the NKJV as "council".

The elders of Israel are mentioned post-exile, positively in [Ezra 6:14](https://www.biblegateway.com/passage/?search=Ezra%206%3A14&version=NKJV) and negatively in [Ezekiel 8](https://www.biblegateway.com/passage/?search=Ezekiel%208&version=NKJV). The deteriorating of the leadership of the elders parallels the rejection of Israel by God, culminating in the destruction of the second temple in 70AD.

----

[^1]: [Exodus 3:18](https://www.biblegateway.com/passage/?search=Exodus%203:18&version=NKJV)
[^2]: [Exodus 24:1](https://www.biblegateway.com/passage/?search=Exodus+24%3A1&version=NKJV), [Numbers 11:16](https://www.biblegateway.com/passage/?search=Numbers%2011%3A16&version=NKJV)
[^3]: [Numbers 11:16-29](https://www.biblegateway.com/passage/?search=numbers+11%3A16-29&version=NKJV)
[^4]: [1 Kings 8:1](https://www.biblegateway.com/passage/?search=1%20Kings%208%3A1&version=NKJV)
[^5]: [Judges 17:6](https://www.biblegateway.com/passage/?search=Judges%2017%3A6&version=NKJV), [Judges 21:25](https://www.biblegateway.com/passage/?search=Judges%2021%3A25&version=NKJV)
[^6]: [1 Samuel 8:4-5](https://www.biblegateway.com/passage/?search=1%20Samuel%208:4-5&version=NKJV)
[^7]: [2 Samuel 3:17-18](https://www.biblegateway.com/passage/?search=2%20Samuel%203:17-18&version=NKJV)
[^8]: [2 Samuel 17:16](https://www.biblegateway.com/passage/?search=2+Samuel+17%3A16&version=NKJV)
[^9]: [Acts 5:21](https://www.biblegateway.com/passage/?search=Acts+5%3A21&version=NKJV). The Word English Bible has a note that "A hendiadys (two different terms referring to a single thing) is likely here (a reference to a single legislative body rather than two separate ones) because the term γερουσίαν (gerousian) is used in both 1 Macc 12:6 and Josephus, Ant. 13.5.8 (13.166) to refer to the Sanhedrin."
[^10]: [Acts 23](https://www.biblegateway.com/passage/?search=Acts%2023&version=NKJV)