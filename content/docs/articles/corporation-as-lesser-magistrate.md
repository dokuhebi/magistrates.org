---
title: The Corporation as Lesser Magistrate
description: A brief history of corporations and their authorities
toc: true
authors: Tom Albrecht
tags: ["history", "magistrates", "orania", "bruderhof"]
categories:
series:
date: '2023-07-29'
lastmod: '2023-07-29'
draft: false
---

## Corporate Backing of the New England Colonies

In the early 17th century, the people of England began to develop strategies on how to settle the new world. It was clear that opportunities existed for profit and expansion of the kingdom, and in 1606, James, the king of England, issued a series of charters to men in England for the purpose of colonizing areas of North America, starting in [Virginia in 1609](https://avalon.law.yale.edu/17th_century/va01.asp) and [New England in 1620](https://avalon.law.yale.edu/17th_century/mass01.asp).

These charters provided the legal foundation for Puritan separatists (i.e. the Pilgrims) to migrate from England to New England, remaining subjects of England, but allowed a large amount of self-determination within the colonies. While the companies that owned the charters were not motivated by Puritan beliefs, the Puritans latched onto the New England charter as an opportunity to live their lives without the persecution they experienced while living in England.

The charters were owned by respective [joint stock companies](https://en.wikipedia.org/wiki/Joint-stock_company), and the New England charters were managed by the [Plymouth Company](https://en.wikipedia.org/wiki/Plymouth_Company) and later the [Council for New England](https://en.wikipedia.org/wiki/Council_for_New_England). The investors financed the company had profits in mind, and while the original settlements in Maine and Massachusetts were financial failures, those colonies encouraged more investment by the Massachusetts Bay Company to form the [Massachusetts May Colony](https://en.wikipedia.org/wiki/Massachusetts_Bay_Colony). While the number of original settlers in Plymouth in 1620 were around 100, the Massachusetts Bay Colony saw around 20,000 colonists migrate in the 1630s. Most of those colonists who volunteered to migrated were also Puritans.

While the history of the settlement of the North America by the English is has been taught since grade school, the financial investment that enabled the establishment of the colonies in the United States is not nearly as well known. I had started doing some research into the financial backing of the colonists, and it's pretty clear that while the king granted charters defining the corporate boundaries of the various ventures, it was the corporations under the charter that funded those ventures and established the expectations for the colonies.

William Bradford's account, [On Plymouth Plantation](https://www.gutenberg.org/cache/epub/24950/pg24950-images.html), describes in details the communications between the Plymouth colony and the investors in England. [Thomas Weston](https://en.wikipedia.org/wiki/Thomas_Weston_(merchant_adventurer)) was one of the financiers for Plymouth Plantation, and while he was related to Puritans by marriage, never became one himself. He sought to profit from the new world through various ventures in New England, Virginia, and Maine, and eventually became an outlaw to the crown due to his unscrupulous business dealings.

During the first few years, the Plymouth colony was in debt to their financiers, but agreed that as ships were dispatched back to England, the colonists would send whatever trade goods they could supply. Bradford recorded letters from England to the colonists chastising the colonists for the lack of goods. Over the years, more ships would come carrying settlers and sent back carrying goods.

Over time, more colonies were established, each tied to a particular investment company. In the century that followed, it was the onerous requirements of the investment companies (i.e. trade from the colonies was only to be shipped to England and nowhere else in Europe) that led to the interposition of the colonies in response to those "abuses and usurpations", also known as the American Revolution.

## The Corporation and Town of Orania

Corporate colonization is not an concept lost to history. In 1990, a group of 40 families formed a corporation (Orania Bestuursdienste) with the goal of purchasing from the South African government the town of [Orania](https://en.wikipedia.org/w/index.php?title=Orania&oldid=1166344271), about 3.5 mi<sup>2</sup> of existing buildings and the surrounding land, all of which was in severe disrepair.

The plan for the establishment of Orania stemmed from the complex relationship between the Afrikaner (Dutch descended) people of South Africa. Their goal for maintaining their Afrikaner culture within South Africa led to a migration of investing families to move to the newly acquired land, although not all the investors made the transition.

Orania is run as a new private company, Vluytjeskraal Share Block, with the residents being shareholders in the company. Wikipedia describes the administration of the town as follows:

> Ownership of plots and houses is in the form of shares in the company, according to a framework known as 'share block' under South African law, similar to the strata title or condominium in other countries. No title deeds are provided, except for agricultural land. Share blocks are linked to portions of the company's real estate property, and the shareholder acquires the right to use property linked to their share block.

The town has had conflicts with the national and regional governments, but has been able to work through those conflicts peacefully through the South African courts. The population has been growing by about 10% per year, and the grown has shown no signs of slowing down.

## The Hutterites and Bruderhof

One final example more close to home in the United States is the Hutterite and Bruderhof communities. While internally, communities in both groups function as a socialist collective, their relationship to the areas where they are reside are very much legal and very much capitalistic. There are about around 400 Hutterite colonies in the United States and Canada and a population of about 40,000. The Bruderhof are smaller, with around 18 communities in the United States, but many others around the world.

The Bruderhof exist as a legal corporation and operating various business ventures for funding their communities. They run a [publishing house](https://www.plough.com), an [engineering company](https://www.rifton.com/), and a [toy company](https://www.communityplaythings.com/about-us/history). The profits from those corporations feed back into the communities where the employees live.

Neither group are isolationist, and have developed economic and personal relationships with the people in their surroundings, but have also built an internal society where their daily work [benefits the community of believers first](https://www.biblegateway.com/passage/?search=Galatians+6%3A10&version=NKJV), then the rest of the world.

## The Corporation as a State-Sanctioned Lesser Magistrate

One of concerns I have with the Lesser Magistrate concept, at least as [defined by Trewhella](../../articles/doctrine-of-the-lesser-magistrates-review) is that his examples are almost exclusively local civil governments sanctioned by a higher authority. As I follow what Trewhella is doing, he is working to educate those existing local civil government on their opportunity and responsibility to intercede on behave of their citizens in the face of oppression from higher authorities. In a country the size of the United States, with 50 states and the 3,142 counties, the oppression is widespread and the supporters are diluted.

While efforts like the [Free State Project](https://www.fsp.org/) have implented an informal strategy of migrating people to New Hampshire, even attempting to impact an entire state as small as New Hampshire is a huge challenge. While they have had some successes, they are asking people to relocate without any assurance that their efforts will be fruitful. In other words, they're moving first, then hoping that their presence will eventually meet their goals versus starting a community with principles embedded from the beginning.

Taking advantage of the powers that corporations have in the United States could provide an alternative strategy, especially for Christians. We have a [long history](https://www.biblegateway.com/passage/?search=Acts+2%3A44-45&version=NKJV) of communities that exist within, but are separated from, their local societies. I am not so naive to think that the result would be instant utopian societies, but clustered communities will offer a protective network for a community when the rest of society ceases to offer that protection.
