---
title: Vintage
draft: false
slogan: Every man by nature is a freeman born; by nature no man cometh out of the womb under any civil subjection to king, prince, or judge.
imgLeft: images/rutherford.jpg
imgRight: images/John-Knox.jpg

weight: 4
widget:
  handler: vintage

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:

  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment:
---