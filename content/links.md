+++
title = "Links"
description = "Links and References"
date = "2023-07-08"
aliases = ["links"]
author = "dokuhebi"
+++

Random links for sites referenced or considered while authoring this page. Linking does not imply agreement with the content or position of the site, but they're worth reading.

## Websites

| Site | Comments |
| ---- | -------- |
| [Defy Tyrants](https://defytyrants.com/) | Articles and Activism on the Doctrine of the Lesser Magistrate |