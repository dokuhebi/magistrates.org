+++
author = "dokuhebi"
title = "Introduction"
date = "2023-07-08"
description = "Introduction to the site"
tags = [
    "intro", "theonomy",
]
+++

I created this blog as a bit of personal therapy to think through my thoughts on civil law, Biblical law, and our obligation as Christians to submit to authority.

I think the question boils down to the following:

# What gives legitimacy to a civil authority?

For background, and to avoid wasting time (mine and yours), I should state my background and assumptions

1. The Bible is the inspired Word of God.
2. God's revelation in the Bible is sufficient for salvation.
3. The laws that God reveals in scripture are not arbitrary, but reflect His nature.

I grew up in the Reformed Protestant Presbyterian tradition, surrounded by many respected men with various positions on Theonomy, Christian Reconstruction, and the applicability of God's law to the Christian. I saw pejoratives like "Antinomian" and "Judaizer" thrown around pretty regularly.

I also learned a lot and grew in my understanding and love for Scripture and the answers to Francis Shaeffer's famous question, "[How Shall We Then Live?](https://www.thegospelcoalition.org/article/schaeffers-how-should-we-now-then-live-40-years-later/)"

The Theonomists took special care to evaluate the jot and tittles, and many volumes were written pouring over the Mosaic laws and evaluating how they can apply today[^1]. They were also very clear that criminal prosecution and judgement were responsibilities given to civil magistrates. In other words, even though murder is a capital crime[^2], the civil rulers were to prosecute the crime. Personal vendettas were not condoned.

The nature of civil government took many forms over the millennia that the Bible was written, from judges to kings to Caesars. The level of conformity to God's laws varied from pure anarchy to totalitarian subjugation. Today, in the United States, we have a very complex levels of civil government with shadows of the Biblical principles on which our modern legal system depends, but those shadows are fading away.

What I never saw from any of the Christian Reconstructions was a Biblical model for the establishment of a civil authority. Reformed theologians acknowledge various government spheres, mainly church, civil, family, and personal, but there are very distinct means by which those authorities are established.

Family government is pretty easy. Marriage naturally leads to children, wives submit to husbands, and children obey parents.

Church government is a little more fuzzy. Yes, we're called to submit to the authority of the church[^3], but how that church establishes itself as an authority is still debated. I am not looking to debate that here, since there are plenty of places to do that elsewhere.

Civil government is, to me, the fuzziest of them all. The Apostle Peter makes a very simple statement in his first letter that believers are to "submit yourselves to every ordinance of man for the Lord’s sake, whether to the king as supreme, or to governors, as to those who are sent by him for the punishment of evildoers and for the praise of those who do good."[^4].

So, what does that mean? How are kings and governments established? Is it by their say so? How do we resolve conflicts between rulers or between earthly authority and God's authority?

God gave me an engineering mindset, and I'm uneasy with undefined standards, especially when it comes to clear cut commands to submit to authority. So, allowing scripture to interpret scripture, join me on this journey of discovery.

I don't expect there to be any rhyme or reason to the order of these posts. It's really a means to get the various concepts out of my head and into words. I'd love your feedback, so please join my [telegram group](https://t.me/+AOY5pLf1-hU5ZGIx) and give me some feedback.

----

[^1]: Gary North's [Tools of Dominion](https://www.garynorth.com/freebooks/docs/372e_47e.htm) and R. J. Rushdoony's [Institutes of Biblical Law](https://www.goodreads.com/en/book/show/1206158) were frequent reads.

[^2]: [Numbers 35:16](https://www.biblegateway.com/passage/?search=Numbers+35%3A16&version=NKJV)

[^3]: [Hebrews 13:17](https://www.biblegateway.com/passage/?search=Hebrews+13%3A17&version=NKJV)

[^4]: [1 Peter 2:13,14](https://www.biblegateway.com/passage/?search=1+Peter+2%3A13-14&version=NKJV)
